/*
 * San Services HN, Jun 17, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts;

import com.sanservices.hn.services.resorts.model.Resort;
import com.sanservices.hn.services.resorts.api.repository.ResortRepository;
import com.sanservices.hn.services.resorts.api.service.ResortService;
import com.sanservices.hn.services.resorts.api.service.ResortServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Raul Argueta
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ResortControllerTest {

    @Autowired
    @Qualifier("memory")
    private ResortRepository inMemoryResortRepository;
    private ResortService resortService;

    @Before
    public void setup() {
        resortService = new ResortServiceImpl(inMemoryResortRepository);
    }

    @Test
    public void ResortNotFoundSandalsTest() {
        Resort smb = resortService.getResortByCode("SMB");
        Assert.assertEquals(true, smb == null);
    }

    @Test
    public void ResortFoundSandalsTest() {
        Resort sat = resortService.getResortByCode("SAT");
        Assert.assertEquals(true, (sat != null));
    }

}
