package com.sanservices.hn.services.resorts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResortsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResortsServiceApplication.class, args);
    }
}
