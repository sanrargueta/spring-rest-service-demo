/*
 * San Services HN, May 15, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.exception.handler;

import java.util.HashMap;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 * @author Raul Argueta
 */
@RestController
@ControllerAdvice
public class NotFoundExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(HttpClientErrorException.NotFound.class)
    public ResponseEntity<Object> handleError(HttpClientErrorException ex, WebRequest request) {
        
        Map<String, String> errorMap = new HashMap<>();
        errorMap.put("detail", "this resource does not exist");
        errorMap.put("error", "110004");
        return ResponseEntity
                .status(ex.getStatusCode())
                .headers(ex.getResponseHeaders())
                .body(errorMap);
    }
}
