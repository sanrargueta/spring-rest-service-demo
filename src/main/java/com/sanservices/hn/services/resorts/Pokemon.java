/*
 * San Services HN, May 2, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author Raul Argueta
 */
@Data
public class Pokemon implements IPokemon, Serializable {

    private static final long serialVersionUID = -6443221328174360460L;

    private int id;
    private String name;
    private int height;

}
