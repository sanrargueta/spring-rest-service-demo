/*
 * San Services HN, Jul 19, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.connection;

import com.sanservices.hn.services.resorts.PokemonMapper;
import com.sanservices.hn.services.resorts.connection.database.DatabaseConfig;
import com.sanservices.hn.services.resorts.connection.database.DatabaseConfigFactory;
import lombok.Data;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 *
 * @author Raul Argueta
 */
@Data
@Configuration
public class DatabaseConnectionManagerImpl implements DatabaseConnectionManager {
    
    @Autowired
    private DatabaseConfigFactory databaseConfigFactory;
    
    @Bean
    @Primary
    public DatabaseConnectionManagerImpl getDatabaseConnectionManager() {
        return new DatabaseConnectionManagerImpl();
    }
    
    @Override
    public SqlSessionFactory getSandalswebSqlSessionFactory() {
        DatabaseConfig sandalsweb = databaseConfigFactory.getSandalsweb();
        
        String driver = "com.mysql.cj.jdbc.Driver";
        String connectionString = String.format("jdbc:mysql://%s:%s/%s", sandalsweb.getServer(), sandalsweb.getPort(), sandalsweb.getDatabase());
        String username = sandalsweb.getUsername();
        String password = sandalsweb.getPassword();
        
        PooledDataSource dataSource = new PooledDataSource(driver, connectionString, username, password);
        dataSource.setPoolMaximumActiveConnections(50);
        dataSource.setPoolMaximumIdleConnections(0);
        dataSource.setPoolPingQuery("SELECT NOW() FROM DUAL");
        
        Environment environment = new Environment("Development", new JdbcTransactionFactory(), dataSource);
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration(environment);
        configuration.addMapper(PokemonMapper.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }
    
}
