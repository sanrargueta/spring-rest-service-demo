/*
 * San Services HN, Jul 19, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.connection.database;

import com.sanservices.hn.services.resorts.util.YamlPropertySourceFactory;
import javax.validation.Valid;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * @author Raul Argueta
 */
@Data
@ConfigurationProperties("database")
@PropertySource(value = "file:./database.yml", factory = YamlPropertySourceFactory.class)
public class DatabaseConfigFactory {

    @Valid
    private DatabaseConfig sandalsweb;

    @Bean
    @Primary
    public DatabaseConfigFactory getDatabaseConfigFactory() {
        return new DatabaseConfigFactory();
    }
}
