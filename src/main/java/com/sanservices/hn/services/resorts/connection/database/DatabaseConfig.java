/*
 * San Services HN, Jul 17, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.connection.database;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 *
 * @author Raul Argueta
 */
@Data
@Component
@Validated
public class DatabaseConfig {

    @NotBlank
    private String server;

    @Positive
    private int port;

    @NotEmpty
    private String database;

    @NotEmpty
    @Length(min = 1)
    private String username;

    @NotEmpty
    @Length(min = 1)
    private String password;
}
