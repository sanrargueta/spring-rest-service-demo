/*
 * San Services HN, Mar 7, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.model;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 *
 * @author Raul Argueta
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ResortStatus {

    /**
     *
     */
    ACTIVE(true),
    /**
     *
     */
    INACTIVE(false);

    private final boolean active;

    private ResortStatus(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

}
