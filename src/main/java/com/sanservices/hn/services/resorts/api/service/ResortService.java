/*
 * San Services HN, May 15, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.api.service;

import com.sanservices.hn.services.resorts.model.Resort;
import java.util.List;

/**
 *
 * @author Raul Argueta
 */
public interface ResortService {

    public List<Resort> getResorts();
    
    public Resort getResortByCode(String rstCode);
}
