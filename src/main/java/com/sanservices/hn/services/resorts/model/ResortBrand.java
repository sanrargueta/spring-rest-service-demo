/*
 * San Services HN, Feb 13, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.model;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 *
 * @author Raul Argueta
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ResortBrand {
    /**
     *
     */
    SANDALS("S", "Sandals"),
    /**
     *
     */
    BEACHES("B", "Beaches"),
    /**
     *
     */
    GRAND_PINEAPPLE("P", "Grand Pineapple"),
/**
     *
     */
    JAMAICAN_VILLAS("V", "Jamaican Villas"),
    /**
     *
     */
    RIO_CHICO("R", "Rio Chico"),
    /**
     *
     */
    UNKNOWN("U", "Unknown");

    private String code;
    private String name;

    private ResortBrand(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }
    
    public String getName() {
        return name;
    }


    public static ResortBrand match(String type) {

        for (ResortBrand brand : ResortBrand.values()) {
            if (brand.getCode().equalsIgnoreCase(type)) {
                return brand;
            }
        }
        return ResortBrand.UNKNOWN;
    }

}
