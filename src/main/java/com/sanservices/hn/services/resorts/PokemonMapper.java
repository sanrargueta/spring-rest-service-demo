/*
 * San Services HN, May 2, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

/**
 *
 * @author Raul Argueta
 */
public interface PokemonMapper {

    @Select("SELECT * FROM Pokemon WHERE id=#{id}")
    @Results(value = {
        @Result(property = "id", column = "id")
        ,@Result(property = "name", column = "identifier")
        ,@Result(property = "height", column = "height")}
    )
    public Pokemon getPokemonById(Integer id);

}
