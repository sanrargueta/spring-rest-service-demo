/*
 * San Services HN, May 16, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.exception;

/**
 *
 * @author Raul Argueta
 */
public class RequestModelValidationException extends Exception {

    public RequestModelValidationException() {
    }

    public RequestModelValidationException(String message) {
        super(message);
    }

    public RequestModelValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestModelValidationException(Throwable cause) {
        super(cause);
    }

    public RequestModelValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
