/*
 * San Services HN, Feb 13, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

/**
 *
 * @author Raul Argueta
 */
@Data
@JsonInclude(Include.NON_NULL)
public class ResortLocation {

    private City city;
    private Country country;
    private State state;

    public ResortLocation(City city, Country country, State state) {
        this.city = city;
        this.country = country;
        this.state = state;
    }

}
