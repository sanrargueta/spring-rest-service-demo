/*
 * San Services HN, Mar 8, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Raul Argueta
 */
@Data
@JsonInclude(Include.NON_NULL)
public class ResortProperty {

    private int numberOfRooms;
    private int numberOfFloors;
    private int numberOfElevators;
    private Date yearBuilt;
    private Date lastRenovation;
    private String checkInTime;
    private String checkOutTime;

    public ResortProperty(int numberOfRooms, int numberOfFloors, int numberOfElevators, Date yearBuilt, Date lastRenovation, String checkInTime, String checkOutTime) {
        this.numberOfRooms = numberOfRooms;
        this.numberOfFloors = numberOfFloors;
        this.numberOfElevators = numberOfElevators;
        this.yearBuilt = yearBuilt;
        this.lastRenovation = lastRenovation;
        this.checkInTime = checkInTime;
        this.checkOutTime = checkOutTime;
    }

}
