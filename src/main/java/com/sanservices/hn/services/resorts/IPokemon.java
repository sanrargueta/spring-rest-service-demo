/*
 * San Services HN, May 2, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts;

/**
 *
 * @author Raul Argueta
 */
public interface IPokemon {

    public int getId();

    public String getName();
    
    public int getHeight();
}
