/*
 * San Services HN, May 15, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.api.repository;

import com.sanservices.hn.services.resorts.model.Resort;
import com.sanservices.hn.services.resorts.model.ResortBrand;
import com.sanservices.hn.services.resorts.model.ResortStatus;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Raul Argueta
 */
@Repository
@Qualifier("memory")
public class InMemoryResortRepositoryImpl implements ResortRepository {

    private final Map<String, Resort> inMemoryStore = new HashMap<>();

    public InMemoryResortRepositoryImpl() {

        inMemoryStore.put("SAT", new Resort("SAT", "Sandals Grande Antigua", "Sandals Grande Antigua", ResortBrand.SANDALS, ResortStatus.ACTIVE));

    }

    @Override
    public List<Resort> getResorts() {
        List<Resort> resorts = new ArrayList<>();
        for (Map.Entry<String, Resort> resort : inMemoryStore.entrySet()) {
            resorts.add(resort.getValue());
        }
        return resorts;

    }

    @Override
    public Resort getResortByCode(String rstCode) {
        return inMemoryStore.get(rstCode.toUpperCase());
    }

}
