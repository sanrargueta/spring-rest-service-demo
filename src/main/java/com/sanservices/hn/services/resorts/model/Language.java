/*
 * San Services HN, Mar 8, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.model;

import java.util.Locale;

/**
 *
 * @author Raul Argueta
 */
public enum Language {
    /**
     *
     */
    EN_US(new Locale("en", "US"), 1),
    /**
     *
     */
    ES_ES(new Locale("es", "ES"), 2);

    private Locale locale;
    private int languageId;

    private Language(Locale locale, int languageId) {
        this.locale = locale;
        this.languageId = languageId;
    }

    private Language(Locale locale) {
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public int getLanguageId() {
        return languageId;
    }

    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }

    public static Language matchByLocale(Locale locale) {
        for (Language resortWebLocale : Language.values()) {
            if (resortWebLocale.getLocale().equals(locale)) {
                return resortWebLocale;
            }
        }
        return null;
    }

}
