/*
 * San Services HN, May 15, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.api.controller.v1;

import com.sanservices.hn.services.resorts.api.service.ResortService;
import com.sanservices.hn.services.resorts.model.Resort;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

/**
 *
 * @author Raul Argueta
 */
@RestController
@Slf4j
@RequestMapping("/v1")
public class ResortController {

    private final ResortService service;

    @Autowired
    public ResortController(ResortService service) {
        this.service = service;
    }

    @RequestMapping(value = "/resorts", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<Object> getResorts(HttpServletRequest request) {

        Map<String, Object> responsePayload = new HashMap<>();
        List<Resort> resorts = service.getResorts();
        List<EntityModel<Resort>> resortEntityList = new ArrayList<>();

        List<String> headers = Collections.list(request.getHeaderNames());
        for (String header : headers) {
            System.out.println("=====================");
            System.out.println("Header: " + header);
            System.out.println("Value: " + request.getHeader(header));
        }

        //Add hypermedia links to each resort entity/resource/
        for (Resort resort : resorts) {
            Link link = linkTo(methodOn(ResortController.class, resort.getCode())
                    .getResortsByCode(null)).withSelfRel();
            resortEntityList.add(new EntityModel(resort, link));//
        }
        responsePayload.put("resorts", resortEntityList);
        return ResponseEntity.status(HttpStatus.OK).body(responsePayload);

    }

    @RequestMapping(value = "/resorts/{rstCode}", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<Object> getResortsByCode(@PathVariable("rstCode") String rstCode) {

        Resort resort = service.getResortByCode(rstCode);

        Link link = linkTo(methodOn(ResortController.class, resort.getCode())
                .getResortsByCode(null)).withSelfRel();

        System.out.println(link.toString());

        EntityModel<Resort> resortEntity = new EntityModel(resort, link);

        if (resort == null) {

            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.add("X-Request-Id", MDC.get("request_uuid"));

            throw HttpClientErrorException.create(
                    HttpStatus.NOT_FOUND,
                    "",
                    new HttpHeaders(headers),
                    null,
                    Charset.defaultCharset());
        }
        return ResponseEntity.status(HttpStatus.OK).body(resortEntity);
    }

}
