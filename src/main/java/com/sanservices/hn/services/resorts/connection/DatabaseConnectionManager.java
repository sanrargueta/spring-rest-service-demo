/*
 * San Services HN, Jul 19, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.connection;

import org.apache.ibatis.session.SqlSessionFactory;

/**
 *
 * @author Raul Argueta
 */
public interface DatabaseConnectionManager {

    public SqlSessionFactory getSandalswebSqlSessionFactory();

}
