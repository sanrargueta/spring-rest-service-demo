/*
 * San Services HN, May 16, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.logging;

import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author Raul Argueta
 */
@Aspect
@Component
@Slf4j
public class RequestLogAspect {

    @Around("@annotation(org.springframework.web.bind.annotation.RequestMapping) && execution(public * *(..))")
    public Object log(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getResponse();

        Object value;
        String requestId = UUID.randomUUID().toString();
        MDC.put("request_uuid", requestId);
        MDC.put("log_request_uuid", String.format("request=\"%s\"", requestId));

        try {

            value = proceedingJoinPoint.proceed();

        } catch (Throwable throwable) {
            throw throwable;
        } finally {
            
            if (response != null) {
                response.setHeader("x-request-id", requestId);
            }

            log.info(
                    "method=\"{}\" uri=\"{}\" ip=\"{}\"",
                    request.getMethod(),
                    request.getRequestURI(),
                    request.getRemoteAddr()
            );
        }

        return value;
    }
}
