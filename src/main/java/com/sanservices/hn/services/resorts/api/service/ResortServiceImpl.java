/*
 * San Services HN, May 15, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.api.service;

import com.sanservices.hn.services.resorts.model.Resort;
import com.sanservices.hn.services.resorts.api.repository.ResortRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 *
 * @author Raul Argueta
 */
@Service
@Qualifier("persistence")
public class ResortServiceImpl implements ResortService {

    private ResortRepository repository;

    @Autowired
    public ResortServiceImpl(@Qualifier("memory") ResortRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Resort> getResorts() {
        return repository.getResorts();
    }

    @Override
    public Resort getResortByCode(String rstCode) {
        return repository.getResortByCode(rstCode);
    }

}
