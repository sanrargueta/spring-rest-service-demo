/*
 * San Services HN, Mar 7, 2019
 *
 * All rights reserved.
 * Copying of this software or parts of this software is a violation
 * of U.S. and international laws and will be prosecuted.
 *
 * Author(s): Raul Argueta ( Software Engineer )
 * 
 *       Raul Argueta
 *      Initial Implementation
 */
package com.sanservices.hn.services.resorts.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 *
 * @author Raul Argueta
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Country {

    private String name;
    private String shortName;
    private String code;

    public Country(String name, String shortName, String code) {
        this.name = name;
        this.shortName = shortName;
        this.code = code;
    }

}
